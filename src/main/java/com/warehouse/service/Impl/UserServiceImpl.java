package com.warehouse.service.Impl;


import com.warehouse.exception.RecordExistsException;
import com.warehouse.exception.RecordNotFoundException;
import com.warehouse.model.User;
import com.warehouse.repository.UserRepository;
import com.warehouse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Override
    public User findUserById(Long id) {
        Optional<User> user = userRepository.findUserById(id);
        if (!user.isPresent()) {
            throw new RecordNotFoundException("User id is not found");
        }
        return user.get();
    }

    @Override
    public void save(User userNew) {
        if (userRepository.existsUserByUserName(userNew.getUserName())) {
            throw new RecordExistsException("Record is exists");
        } else if (userRepository.existsUserByEmail(userNew.getEmail())) {
            throw new RecordExistsException("Record is exists");
        }
        userRepository.save(userNew);
    }

    @Override
    public void update(User userUpdate) {
        Optional<User> user = userRepository.findUserById(userUpdate.getId());
        if (!user.isPresent()) {
            throw new RecordNotFoundException("Can not find you recorded yet");
        }
        userRepository.save(userUpdate);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

}
