package com.warehouse.service.Impl;

import com.warehouse.exception.RecordExistsException;
import com.warehouse.exception.RecordNotFoundException;
import com.warehouse.model.Category;
import com.warehouse.repository.CategoryRepository;
import com.warehouse.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Category findCategoryById(Long id) {
        Optional<Category> category = categoryRepository.findCategoryById(id);
        if (!category.isPresent()) {
            throw new RecordNotFoundException("Category is not found");
        }
        return category.get();
    }

    @Override
    public void save(Category category) {
        if (categoryRepository.existsCategoryByName(category.getName())) {
            throw new RecordExistsException("Record is exists");
        } else if (categoryRepository.existsCategoryByCode(category.getCode())) {
            throw new RecordExistsException("Record is exists");
        }
        categoryRepository.save(category);
    }

    @Override
    public void update(Category categoryUpdate) {
        Optional<Category> category = categoryRepository.findCategoryById(categoryUpdate.getId());
        if (!category.isPresent()) {
            throw new RecordNotFoundException("Category is not found");
        }
        categoryRepository.save(categoryUpdate);
    }

    @Override
    public void deleteById(Long id) {
        categoryRepository.deleteById(id);
    }
}
