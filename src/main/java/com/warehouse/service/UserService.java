package com.warehouse.service;

import com.warehouse.model.User;

public interface UserService {
    User findUserById(Long id);

    void save(User user);

    void update(User user);

    void deleteById(Long id);
}
