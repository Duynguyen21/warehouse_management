package com.warehouse.service;

import com.warehouse.model.Category;

public interface CategoryService {
    Category findCategoryById(Long id);

    void save(Category category);

    void update(Category category);

    void deleteById(Long id);
}
