package com.warehouse.repository;

import com.warehouse.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Optional<Category> findCategoryById(Long id);

    Boolean existsCategoryByName(String name);

    Boolean existsCategoryByCode(String Code);

    void deleteById(Long id);
}
