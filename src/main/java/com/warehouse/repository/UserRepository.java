package com.warehouse.repository;

import com.warehouse.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserById(Long id);

    Boolean existsUserByUserName(String userName);

    Boolean existsUserByEmail(String email);

    void deleteById(Long id);
}
