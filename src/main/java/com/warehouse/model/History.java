package com.warehouse.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "histories")
@Setter
@Getter
public class History implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private String actionName;

    @NonNull
    private Long type;

    @NonNull
    private Long quantity;

    @NonNull
    private BigDecimal price;

    @NonNull
    private Date createdDate;

    @NonNull
    private Date updateDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

}
