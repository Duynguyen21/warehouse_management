package com.warehouse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "products")
@Setter
@Getter
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min = 6)
    private String name;

    @NonNull
    private String code;

    @NonNull
    @Size(min = 6)
    private String description;

    @NonNull
    private String imgUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

    @OneToMany(mappedBy = "product")
    private Set<ProductInStock> productInStocks = new HashSet<>();

    @OneToMany(mappedBy = "product")
    private Set<History> history = new HashSet<>();

    @OneToMany(mappedBy = "product")
    private Set<Invoice> invoice = new HashSet<>();

}
