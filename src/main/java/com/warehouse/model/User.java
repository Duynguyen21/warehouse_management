package com.warehouse.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.Size;


@Entity
@Setter
@Getter
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    @Size(max = 50)
    private String firstName;
    @Size(max = 50)
    private String lastName;
    @NonNull
    @Size(min = 6, max = 50)
    private String userName;
    @NonNull
    @Size(min = 15, max = 70)
    private String email;
    @NonNull
    @Size(min = 6)
    private String password;
    @NonNull
    private Date createdDate;
    @NonNull
    private Date updateDate;
}
